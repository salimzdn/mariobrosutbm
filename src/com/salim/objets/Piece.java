package com.salim.objets;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Piece extends Objet{

	private int position=0;
	private boolean chope=false;
	
	public Piece(int x, int y) {
		
		super(x, y, 17, 15, "piece1");

	}
	
	//Getter
	
	public boolean isChope() {
		return chope;
	}
	
	//Setter

	public void setChope(boolean chope) {
		this.chope = chope;
	}
	
	//M�thodes
	

	public Image rotation()
	{
		String str = null;
		ImageIcon ico;
		Image img;
		
		if(this.position==0 ||this.position==1)
		{
			str="/Images/piece1.png";
		}
		else if(this.position==2 ||this.position==3||this.position==10 ||this.position==11)
		{
			str="/Images/piece2.png";
		}
		else if(this.position==4 ||this.position==5||this.position==8 ||this.position==9)
		{
			str="/Images/piece3.png";
		}
		else if(this.position==6 ||this.position==7)
		{
			str="/Images/piece4.png";
		}
		
		if(this.position!=11)
		{
			this.position=this.position+1;
		}
		else
		{
			this.position=0;
		}
		
		ico= new ImageIcon(getClass().getResource(str));
		img=ico.getImage();
		return img;
		
	}

	
	
	

}
