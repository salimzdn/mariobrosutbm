package com.salim.objets;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.salim.jeu.Main;
import com.salim.utilitaire.Utilitaire;

public class Objet {
	
	private  int x,y;
	private int largeur,hauteur;
	private Image imgObjet;
	private ImageIcon icoObjet;
	String nom;

	private boolean chope;
	


	public Objet (int x, int y , int hauteur,int largeur,String nom){
		
		this.x=x;
		this.y=y;
		this.hauteur=hauteur;
		this.largeur=largeur;
		this.icoObjet=new ImageIcon(getClass().getResource("/Images/"+nom+".png"));
		this.imgObjet=this.icoObjet.getImage();
		this.setChope(false);
		this.nom=nom;
	}


	//Getters
	
	
	
	
	public  int getY() {
		return y;
	}
	
	public   int getX() {
		return x;
	}
	
	public int getLargeur() {
		return largeur;
	}
	
	public  int getHauteur() {
		return hauteur;
	}
	
	public String getNom() {
		return nom;
	}

	public Image getImgObjet() {
		return imgObjet;
	}
	
	public boolean isChope() {
		return chope;
	}
	
	//Setters

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
 
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	public void setChope(boolean chope) {
		this.chope = chope;
	}
	

	
	//M�thodes
	
	public void deplacement(){
		if (Utilitaire.niveau.getxPos()>=0)
		{
			this.x=this.x-Utilitaire.niveau.getDx();
		}
	}


	
}
