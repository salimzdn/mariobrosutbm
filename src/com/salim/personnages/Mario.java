package com.salim.personnages;

import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;

import com.salim.jeu.Main;
import com.salim.objets.Objet;
import com.salim.objets.Piece;
import com.salim.utilitaire.Utilitaire;

public class Mario extends Personnage{


	private ImageIcon icoMario;
	private Image imgMario;
	private boolean saut;
	private int timeSaut;//Regle le temps de saut 
	private boolean proche=false;
	private boolean sautePNJ;
	private int hauteurSaut=50;
	private int tabSol[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	private boolean victoire=false;
	
	public Mario(int x, int y)
	{
		super(x, y, 28, 50,"mario");
		icoMario= new ImageIcon(getClass().getResource("/Images/marioMarcheDroite.png"));
		imgMario=this.icoMario.getImage();
		this.saut=false;
		this.timeSaut=0;
	}



//Getter
	
	
	public boolean isSaut() {
		return saut;
	}

	public boolean isSautePNJ() {
		return sautePNJ;
	}

	public int getTimesaut() {
		return timeSaut;
	}

	public Image getImgMario() {
		return imgMario;
	}
	public boolean isProche() {
		return proche;
	}


//Setter

	public void setSaut(boolean saut) {
		this.saut = saut;
	}

	public void setProche(boolean proche) {
		this.proche = proche;
	}

	public void setSautePNJ(boolean sautePNJ) {
		this.sautePNJ = sautePNJ;
	}
//M�thodes
	
	public Image saute(){
		
		ImageIcon ico;
		Image img;
		String str = null;
	
		this.timeSaut ++;
	
		if(this.timeSaut<=this.hauteurSaut){//Mario Saute
			
				if(this.getY()>Utilitaire.niveau.getHauteurPlafond())
					{this.setY(this.getY()-4);}
				else
					{this.timeSaut=this.hauteurSaut;}
				
				if(this.isSensAvant()==true)
					{str="/Images/marioSautDroite.png";}
				else
					{str="/Images/marioSautGauche.png";}
			}
		else if(this.isSautePNJ()==true && this.timeSaut<=this.hauteurSaut)
		{
			if(this.getY()>Utilitaire.niveau.getHauteurPlafond())
				{this.setY(this.getY()-4);}
			else
				{this.timeSaut=this.hauteurSaut;}
			if(this.isSensAvant()==true)
				{str="/Images/marioSautDroite.png";}
			else
				{str="/Images/marioSautGauche.png";}
		}
		else if(this.getY()+this.getHauteur()<Utilitaire.niveau.getySOl())//Mario redessent
			{	this.setY(this.getY()+1);
				if(this.isSensAvant()==true){str="/Images/marioSautDroite.png";}
				else{str="/Images/marioSautGauche.png";}
			}
		else{//MArio au sol
			if(this.isSensAvant()==true){str="/Images/marioArretDroite.png";}
			else{str="/Images/marioArretGauche.png";}
			this.saut=false;
			this.timeSaut=0;
			if(Utilitaire.niveau.isVictoire()==true)
			{
				this.victoire=true;
			}
		}
		
		ico=new ImageIcon(getClass().getResource(str));
		img=ico.getImage();
			
		return img;
		
	}
	
	public void contact(Objet objet,int i){
		
		
		int j=0,sol=0;
		
		
		while(sol==0 && j<Utilitaire.niveau.tabObjets.size())		{
			if(this.tabSol[j]==1)
			{
				sol=1;
			}
			j++;
		}
		
		
		if(super.ContactAvant(objet)==true&& this.isSensAvant()==true//contact � droite
			|| super.ContactArri�re(objet)==true && this.isSensAvant()==false)//contact � gauche
		{
			Utilitaire.niveau.setDx(0);
			this.setMarche(false);
		}
		
		
		if(super.ContactDessous(objet)==true)//contact en dessous
		{
			Utilitaire.niveau.setySOl(objet.getY());//Mario saute sur un objet alors l'objet devient le sol
			this.tabSol[i]=1;
			System.out.println("***********"+tabSol);
		}
		else if (super.ContactDessous(objet)==false)//Pas de contact en dessous donc on retombe sur le sol
		{		
			this.tabSol[i]=0;
			if(sol==0)
			{
				Utilitaire.niveau.setySOl(293);//hauteur sol initial
				
				if(this.getY()+this.getHauteur()<Utilitaire.niveau.getySOl()&&this.saut==false)//Mario redessent
				{	
					this.setY(this.getY()+1);
				}
			}	
		}
		
		
		if(super.ContactDessus(objet)==true)//contact au dessus
		{
			Utilitaire.niveau.setHauteurPlafond(objet.getY()+objet.getHauteur());//On deplace le plafond
			if(objet.getNom()=="intpoint")
			{
				/*Utilitaire.niveau.ipiece[0]= new Piece(objet.getX(),objet.getY()-30);
				Utilitaire.niveau.tabPieces.add(Utilitaire.niveau.ipiece[0]);*/
				
				System.out.println("------ Contact int ------");
			}
		}
		else if (super.ContactDessus(objet)==false&&this.saut==false)
		{
			Utilitaire.niveau.setHauteurPlafond(0);//On remet le plafond normal
		}
	}
	
	public void contact(Personnage PNJ){
		if(PNJ.isVivant()==true && (super.ContactAvantPNJ(PNJ)==true//contact � droite
			|| super.ContactArri�rePNJ(PNJ)==true ))//contact � gauche
		{
			Utilitaire.niveau.setDx(0);
			this.setMarche(false);
			this.setVivant(false);
		}
		
		if(super.ContactDessousPNJ(PNJ)==true)//contact en dessous
		{
			if (PNJ.isVivant()==true && this.isVivant()==true)
			{
				if(this.isSaut()==true)
				{
					this.setSaut(false);
				}
				this.setSaut(true);
				PNJ.setVivant(false);
				System.out.println("saute");
				Utilitaire.niveau.setScore(Utilitaire.niveau.getScore()+200);
			}
			else if(PNJ.isVivant()==false && PNJ.getNom()=="tortue")
			{
				PNJ.setDoubleTouch(true);
				this.setSautePNJ(true);
			}
		}
	}
	
	public void contactPiece(Objet piece)
	{
		if((this.ContactArri�re(piece)==true || this.ContactAvant(piece)==true||this.ContactDessous(piece)==true
			|| this.ContactDessus(piece)==true) && piece.isChope()==false)
		{
			Utilitaire.niveau.setScore(Utilitaire.niveau.getScore()+100);
			piece.setChope(true);
		}
	}
	
	public void contactLave(Objet lave)
	{
		if((this.ContactArri�re(lave)==true || this.ContactAvant(lave)==true||this.ContactDessous(lave)==true
				|| this.ContactDessus(lave)==true) && this.isVivant()==true)
			{
				Utilitaire.niveau.setScore(0);
				this.setVivant(false);
			}
	}
	
	public void contactInt(Objet obj)
	{
		if(this.ContactDessous(obj)==true)
			{
				
			}
	}
	


	public Image victoire()
	{
		Image img = null;
		
		if(this.victoire==false)
		{
			img=this.saute();
		}
		else
		{
			if(Utilitaire.niveau.getxPos()<Utilitaire.niveau.getxFin())
			{
				System.out.println("  - position  -   "+Utilitaire.niveau.getxPos());
				//Utilitaire.niveau.setDx(1);
				img=this.marche("mario",35);
			}
			else
			{
				System.out.println("fin");
				Utilitaire.niveau.setDx(0);
			}
			
		}
		return img;
	}


	
	

	
}
