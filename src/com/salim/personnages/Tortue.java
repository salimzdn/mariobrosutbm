package com.salim.personnages;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.salim.jeu.Main;
import com.salim.objets.Objet;

public class Tortue extends Personnage implements Runnable{
	
	private ImageIcon icoTortue;
	private Image imgTortue;
	private final int PAUSE=15;
	private int dxTortue;
	
	public Tortue(int x, int y) {
		
		super(x, y, 43, 50,"tortue");
		icoTortue= new ImageIcon(getClass().getResource("/Images/tortueMarcheDroite.png"));
		imgTortue=this.icoTortue.getImage();
		super.setSensAvant(true);
		super.setMarche(true);
		this.dxTortue=1;
		
		Thread chronoTortue= new Thread (this);
		chronoTortue.start();
	}
	
	//M�thodes
	
	public void run() {
		try {Thread.sleep(20);} //Pour attendre 20ms pour que le d�cor soit d�ssiner
		catch (InterruptedException e) {e.printStackTrace();}
		
		while(true)
		{
			this.bouge();
			
			try {Thread.sleep(PAUSE);} 
			catch (InterruptedException e) {e.printStackTrace();}
			
		}
		
	}
	
	public void bouge(){
		if (this.isVivant()==true)
		{
			if(super.isSensAvant()==true){this.dxTortue=1;}
			else {this.dxTortue=-1;}
			
		}
		else
		{
			if(super.doubleTouch==false)
			{
				this.dxTortue=0;
			}
			
			else
			{
				if(super.isSensAvant()==true){this.dxTortue=5;}
				else {this.dxTortue=-5;}
			}
		}
		super.setX(super.getX()+this.dxTortue);
	}

	
	

}
