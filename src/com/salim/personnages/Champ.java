package com.salim.personnages;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Champ extends Personnage implements Runnable{
	
	private Image imgChamp;
	private ImageIcon icoChamp;
	private final int PAUSE=15;
	private int dxChamp;
	
	public Champ(int x, int y) {
		super(x, y, 27, 30,"champ");
		super.setSensAvant(true);
		super.setMarche(true);
		this.dxChamp=1;
		this.icoChamp=new ImageIcon(getClass().getResource("/Images/champArretDroite.png"));
		this.imgChamp=this.icoChamp.getImage();
		
		Thread chronoChamp= new Thread (this);
		chronoChamp.start();
	}

	
	//Getter
	
	public Image getImgChamp() {
		return imgChamp;
	}

	
	//M�thodes
	
	public void run() {
		try {Thread.sleep(20);} //Pour attendre 20ms pour que le d�cor soit d�ssiner
		catch (InterruptedException e) {e.printStackTrace();}
		
		while(true)
		{
			this.bouge();
			
			try {Thread.sleep(PAUSE);} 
			catch (InterruptedException e) {e.printStackTrace();}
			
		}
		
	}
	
	public void bouge(){
		
		if (this.isVivant()==true)
		{
			if(super.isSensAvant()==true){this.dxChamp=1;}
			else {this.dxChamp=-1;}
			
		}
		else
		{
			this.dxChamp=-0;
		}
		super.setX(super.getX()+this.dxChamp);
	}
}
