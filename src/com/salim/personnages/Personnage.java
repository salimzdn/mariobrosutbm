package com.salim.personnages;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.salim.jeu.Main;
import com.salim.objets.Objet;
import com.salim.utilitaire.Utilitaire;



public class Personnage {
	
	
	private int x,y;//position du perso
	private int largeur, hauteur;//dimensions du perso
	private boolean marche;//Est-ce que le perso marche ou nan
	private boolean sensAvant;//Le personnage va vers la droite=true
	private int vitesse;//Vitesse selon type de personnage
	private boolean vivant;
	private String nom;
	protected boolean doubleTouch=false;
	
	//Constructeur
	
	
	public Personnage(int x2, int y2, int largeur2, int hauteur2,String nom) {
		// TODO Auto-generated constructor stub
		
		this.x=x2;
		this.y=y2;
		this.hauteur=hauteur2;
		this.largeur=largeur2;
		this.marche=false;
		this.sensAvant=true;
		this.vitesse=0;
		this.vivant=true;
		this.nom=nom;
		
	}
	
	//Getters
	

	public String getNom() {return nom;}

	public boolean isDoubleTouch() {
		return doubleTouch;
	}

	public int getLargeur() {return largeur;}
	
	public int getHauteur() {return hauteur;}
	
	public int getX() {	return x;}
	
	public int getY() {	return y;}
	
	public boolean isMarche() {	return marche;}
	
	public int getVitesse() {return vitesse;}
	
	public boolean isSensAvant() {return sensAvant;}
	
	public boolean isVivant() {return vivant;}
	
	//Setters


	public void setDoubleTouch(boolean doubleTouch) {
		this.doubleTouch = doubleTouch;
	}
	
	public void setX(int x) {this.x = x;}

	public void setY(int y) {this.y = y;}

	public void setMarche(boolean marche) {	this.marche = marche;}

	public void setSensAvant(boolean sensAvant) {this.sensAvant = sensAvant;}

	public void setVitesse(int vitesse) {this.vitesse = vitesse;}
	
	public void setVivant(boolean vivant) {this.vivant = vivant;}
	
	//M�thodes
	
	
	public Image marche (String nom, int compteur ){//Renvoie l'image du perso qui marche
		String str = null;
		ImageIcon ico;
		Image img;
		if(this.vivant==true)
		{
			if (this.marche==false)//Cas o� il ne marche pas
			{
				if (this.sensAvant==true)//En arr�t vers la droite
				{
					str="/Images/"+nom+"ArretDroite.png";
				}
				else//En arr�t vers la gauche
				{
					str="/Images/"+nom+"ArretGauche.png";
				}
			}
			else //Cas o� il marche
			{
				this.vitesse++;//On augmente la variable vitesse de 1
				if(this.vitesse/compteur==0)//Moment o� l'on rebesse la jambe du Personnage pour cr�er un mouvement de jambe
				{
					if (this.sensAvant==true)
					{
						str="/Images/"+nom+"ArretDroite.png";//Quand il va vers la droite
					}
					else
					{
						str="/Images/"+nom+"ArretGauche.png";//Quand il va vers la gauche
					}
					
				}
				else{//Moment o� l'on monte la jambe du Personnage pour cr�er un mouvement de jambe
					if (this.sensAvant==true)
					{
						str="/Images/"+nom+"MarcheDroite.png";//Quand il va vers la droite
					}
					else
					{
						str="/Images/"+nom+"MarcheGauche.png";//Quand il va vers la gauche
					}
				if (this.vitesse==2*compteur)//On aura alors eu la jambe en l'air puis au sol puis en l'air
					this.vitesse=0;//On remet la vitesse � 0 pour que le mouvement se r�p�te comme pr�cedement
				}			
			}
			//On a le nom de l'image , il faut la renvoyer
		}
		else//Quand le personnage est mort
		{
			if(nom=="mario")//Pour Mario
			{
				if(Utilitaire.niveau.mario.getY()>-500)
				{
					System.out.println("mort de mario");
					Utilitaire.niveau.mario.setY(Utilitaire.niveau.mario.getY()-1);
				}
				str="/Images/"+nom+"Meurt.png";
			}
			else if(nom=="champ")//Pour un champignon
			{
				if (this.sensAvant==true)
				{
					str="/Images/"+nom+"EcraseDroite.png";
				}
				else 
				{
					str="/Images/"+nom+"EcraseGauche.png";
				}
			}
			else{str="/Images/"+nom+"Fermee.png";}//Pour une tortue
		}
		
		ico= new ImageIcon(getClass().getResource(str));
		img=ico.getImage();
		return img;
	}
	
	public boolean ContactAvant (Objet objet){
		boolean rep = true;
	
		if (this.x + this.largeur<objet.getX()|| this.x + this.largeur>objet.getX()+5|| 
			this.y+this.hauteur<=objet.getY() || this.y>=objet.getY()+objet.getHauteur())
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
		{
			rep= false;
		}
		else {
			rep= true;
		}


		return rep;
		
	}
	
	public boolean ContactArri�re(Objet objet){
	
		boolean rep = true;
		
		if (this.x >objet.getX()+objet.getLargeur()|| this.x +this.largeur<objet.getX()+objet.getLargeur()-5|| 
			this.y+this.hauteur<=objet.getY() || this.y>=objet.getY()+objet.getHauteur())
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
		{
			rep= false;
		}
		else {
			rep= true;
		}
		
		return rep;
		
	}
	
	public boolean ContactDessus (Objet objet){
		
		boolean rep = true;
		
		if (this.x +this.largeur<objet.getX()+5|| this.x >objet.getX()+objet.getLargeur()-5|| 
			this.y<objet.getY()+objet.getHauteur() || this.y>objet.getY()+objet.getHauteur()+5)
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
				{rep= false;}
		else 	{rep= true;}
		
		
		return rep;
		
	}
	
	public boolean ContactDessous (Objet objet){
		
		boolean rep;
		
		if (this.x +this.largeur<objet.getX()+5|| this.x >objet.getX()+objet.getLargeur()-5|| 
			this.y+this.hauteur<objet.getY() || this.y+this.hauteur>objet.getY()+5)
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
				{rep= false;}
		else 	{rep= true;}
		
		
		return rep;
		
	}
	
	public boolean proche(Objet objet){
		if((this.x>objet.getX() && this.x<objet.getX()+objet.getLargeur())
				||(this.x+this.largeur+5>objet.getX() && this.x+this.largeur<objet.getX()+objet.getLargeur()))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public void deplacement(){
		if (Utilitaire.niveau.getxPos()>=0)
		{
			this.x=this.x-Utilitaire.niveau.getDx();
		}
		
		if(this.isVivant()==false)
		{
			this.setY(275);
		}
	}
	
	public void contact(Objet objet){
		
		if(this.ContactAvant(objet)==true&& this.isSensAvant()==true)//contact � droite
			 
		{
			this.setSensAvant(false);
		}
	
		else if (this.ContactArri�re(objet)==true && this.isSensAvant()==false)//contact � gauche.
		{
			this.setSensAvant(true);
			
		}
	}

	
	protected boolean ContactAvantPNJ (Personnage PNJ){
		boolean rep = true;
	
		if (this.x + this.largeur<PNJ.getX()|| this.x + this.largeur>PNJ.getX()+5|| 
			this.y+this.hauteur<=PNJ.getY() || this.y>=PNJ.getY()+PNJ.getHauteur())
		{
			rep= false;
		}
		else {
			rep= true;
		}


		return rep;
		
	}
	
	protected boolean ContactArri�rePNJ(Personnage PNJ){
		
		boolean rep = true;
		
		if (this.x >PNJ.getX()+PNJ.getLargeur()|| this.x +this.largeur<PNJ.getX()+PNJ.getLargeur()-5|| 
			this.y+this.hauteur<=PNJ.getY() || this.y>=PNJ.getY()+PNJ.getHauteur())
		{
			rep= false;
		}
		else {
			rep= true;
		}
		
		return rep;
		
	}
	
	protected boolean ContactDessusPNJ (Personnage PNJ){
		
		boolean rep = true;
		
		if (this.x +this.largeur<PNJ.getX()+5|| this.x >PNJ.getX()+PNJ.getLargeur()-5|| 
			this.y<PNJ.getY()+PNJ.getHauteur() || this.y>PNJ.getY()+PNJ.getHauteur()+5)
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
				{rep= false;}
		else 	{rep= true;}
		
		
		return rep;
		
	}
	
	protected boolean ContactDessousPNJ (Personnage PNJ){
		
		boolean rep = true;
		
		if (this.x +this.largeur<PNJ.getX()|| this.x >PNJ.getX()+PNJ.getLargeur()|| 
			this.y+this.hauteur<PNJ.getY() || this.y+this.hauteur>PNJ.getY())
			//Mario trop � gauche, trop � droite, trop haut, trop bas alors pas contact
				{rep= false;}
		else 	{rep= true;}
		
		
		return rep;
		
	}
	
	public boolean prochePNJ(Personnage PNJ){
		if((this.x>PNJ.getX()-10 && this.x<PNJ.getHauteur()+10+PNJ.getLargeur())
				||(this.x+this.largeur>PNJ.getX()-10 && this.x+this.largeur<PNJ.getX()+PNJ.getLargeur()+50))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
public void contactPNJ(Personnage PNJ){
		
		if(this.ContactAvantPNJ(PNJ)==true&& this.isSensAvant()==true)//contact � droite
			 
		{
			this.setSensAvant(false);
			PNJ.setSensAvant(true);
		}
	
		else if (this.ContactArri�rePNJ(PNJ)==true && this.isSensAvant()==false)//contact � gauche.
		{
			this.setSensAvant(true);
			PNJ.setSensAvant(false);
		}
	}




}
