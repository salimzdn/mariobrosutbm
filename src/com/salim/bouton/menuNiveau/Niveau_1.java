package com.salim.bouton.menuNiveau;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import com.salim.bouton.Bouton;
import com.salim.utilitaire.EtapeJeu;
import com.salim.utilitaire.Utilitaire;

public class Niveau_1 extends Bouton{

	
	private Image state1,state2;
	
	public Niveau_1() {
			
			super("Niveau 1", new Rectangle(320, 230, 200, 40), 45, 32, Color.RED, new Font("Arial", Font.BOLD, 30),EtapeJeu.MENU_NIVEAU);
			this.state1 = new ImageIcon(getClass().getResource("/Images/buttom_1_0.png")).getImage();
			this.state2 = new ImageIcon(getClass().getResource("/Images/buttom_1_1.png")).getImage();
			
			super.setTexture(this.state1);
		}
	
		protected void enter() {
			super.setTexture(this.state2);
			
		}
	
		@Override
		protected void exit() {
			super.setTexture(this.state1);
			
		}
	
		@Override
		protected void clickEvent() {
			Utilitaire.etat = EtapeJeu.NIVEAU_1;
			/*Audio.playSound("/audio/clic.wav");
			MarioUtils.niveau.getTimeur().start();*/
			
		}
}
