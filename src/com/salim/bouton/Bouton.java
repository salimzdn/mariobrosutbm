package com.salim.bouton;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import com.salim.bouton.menuNiveau.Niveau_1;
import com.salim.bouton.menuPrincipale.*;
import com.salim.utilitaire.EtapeJeu;
import com.salim.utilitaire.Utilitaire;

public abstract class Bouton {

	/*
	 * 
	 * PETITE CLASS CADEAU DE MA PROPRE LIBRAIRIE. PRENDS EN SOIN ! :)
	 * 
	 */
	
	public static List<Bouton> getButtom = new ArrayList<Bouton>();
	
	private String title;
	private Rectangle hitbox;
	private int[] titleLocation;
	private Image texture;
	private Color colorBouton ,colorTitle;
	private Font font;
	private EtapeJeu etat;
	
	protected Bouton(String title,Rectangle hitbox,int x,int y, Color colorTitle,Font font, EtapeJeu etat)
	{
		this.title = title;
		this.hitbox = hitbox;
		this.titleLocation = new int[]{hitbox.x+x, hitbox.y+y};
		this.colorTitle = colorTitle;
		this.font = font;
		this.etat = etat;
	}
	
	//*** GETTERS ***//
	protected String getTitle(){
		return this.title;
	}
	
	protected Rectangle getHitbox(){
		return this.hitbox;
	}
	
	protected int getTitleX(){
		return this.titleLocation[0];
	}

	protected int getTitleY(){
		return this.titleLocation[1];
	}
	
	public Image getTexture(){
		return this.texture;
	}
	
	protected Color getColorBouton(){
		return this.colorBouton;
	}
	
	protected Color getTitleColor(){
		return this.colorTitle;
	}
	
	protected Font getFont(){
		return this.font;
	}
	
	protected EtapeJeu getState(){
		return this.etat;
	}
	
	
	//*** SETTERS ***//
	protected void setTitle(String title){
		this.title = title;
	}
	
	protected void setHitbox(Rectangle hitbox){
		this.hitbox = hitbox;
	}
	
	protected void setLocationTitle(int x, int y){
		this.titleLocation = new int[]{this.getHitbox().x+x,this.getHitbox().y+y}; 
	}
	
	protected void setColorBouton(Color colorBouton){
		this.colorBouton = colorBouton;
	}
	
	protected void setTexture(Image texture){
		this.texture = texture;
	}
	
	protected void setColorTitle(Color colorTitle){
		this.colorTitle = colorTitle;
	}
	
	protected void setFont(Font font){
		this.font = font;
	}
	
	protected void setState(EtapeJeu state){
		this.etat = state;
	}
	
	//*** RENDU GRAPHICS ***//
	public void render(Graphics2D gdd){
		if(!this.getState().equals(Utilitaire.etat)) return;
		
		if(this.getTexture() == null){
			gdd.setColor(this.getColorBouton());
			gdd.fill(this.getHitbox());
		}else gdd.drawImage(this.getTexture(), this.getHitbox().x, this.getHitbox().y, this.getHitbox().width, this.getHitbox().height, null);
		
		gdd.setFont(this.getFont());
		gdd.setColor(this.getTitleColor());
		gdd.drawString(this.getTitle(), this.getTitleX(), this.getTitleY());
	}
	
	public void dessin( Graphics2D gdd)
	{
		gdd.drawImage(this.getTexture(), this.getHitbox().x, this.getHitbox().y, this.getHitbox().width, this.getHitbox().height, null);
		
		gdd.setFont(this.getFont());
		gdd.setColor(this.getTitleColor());
		gdd.drawString(this.getTitle(), this.getTitleX(), this.getTitleY());
	}
	
	//*** LORSQUE LE JOUEUR SURVOLE LE BOUTON ***//
	public void checkMouse(Rectangle mouseHitbox){
		if(!this.getState().equals(Utilitaire.etat)) return;
		
		if(mouseHitbox.intersects(this.getHitbox())) this.enter();
		else this.exit();
	}
	
	//*** LORSQUE LE JOUEUR CLIQUE SUR LE BOUTON ***//
	public void checkClick(Rectangle mouseHitbox){
		if(!this.getState().equals(Utilitaire.etat)) return;
		if(mouseHitbox.intersects(this.getHitbox())) this.clickEvent();
	}
	
	protected abstract void enter();
	protected abstract void exit();
	protected abstract void clickEvent();
	
	
	//*** IMPORTANT : ENREGISTRER TOUT VOS BOUTONS ICI !!! ***//
	public static void registerButtom(){
		
		getButtom.add(new Options());
		getButtom.add(new Jouer());
		getButtom.add(new Niveau_1());
	}
}
