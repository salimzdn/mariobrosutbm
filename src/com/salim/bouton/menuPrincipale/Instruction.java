package com.salim.bouton.menuPrincipale;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import com.salim.bouton.Bouton;
import com.salim.utilitaire.EtapeJeu;
import com.salim.utilitaire.Utilitaire;

public class Instruction extends Bouton{

private Image state1, state2;
	
	public Instruction() {
		
		super("Instructions", new Rectangle(320, 130, 200, 40), 45, 32, Color.RED, new Font("Arial", Font.BOLD, 30),EtapeJeu.MENU_PRINCIPAL);
		this.state1 = new ImageIcon(getClass().getResource("/Images/buttom_1_0.png")).getImage();
		this.state2 = new ImageIcon(getClass().getResource("/Images/buttom_1_1.png")).getImage();
		
		super.setTexture(this.state1);
	}

	@Override
	protected void enter() {
		super.setTexture(this.state2);
		
	}

	@Override
	protected void exit() {
		super.setTexture(this.state1);
		
	}

	@Override
	protected void clickEvent() {
		Utilitaire.etat = EtapeJeu.INSTRUCTION;
		/*Audio.playSound("/audio/clic.wav");
		MarioUtils.niveau.getTimeur().start();*/
		
	}


}
