package com.salim.jeu;

public class Temps implements Runnable{

	private final int PAUSE =1000;
	private int temps;
	
	//Constructeur
	
	public Temps(){
		this.setTemps(0);
		Thread compteARebours=new Thread(this);
		compteARebours.start();
	}
	
	//Getter
	
	public int getTemps() {
		return temps;
	}

	
	//Setter
	
	public void setTemps(int temps) {
		this.temps = temps;
	}
	
	//M�thode
	
	public void run() {
		
		while(true)
		{
			try{Thread.sleep(PAUSE);}
			catch(InterruptedException e){}
			this.setTemps(this.getTemps() + 1);
			
		}
	}

	

}
