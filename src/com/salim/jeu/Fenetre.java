package com.salim.jeu;

import javax.swing.JFrame;

import com.salim.utilitaire.Utilitaire;


public class Fenetre extends JFrame{
	
	//*** CONSTRUCTEUR ***//
		public Fenetre() {
			super.setTitle("Mario");
			super.setSize(700, 360);
			super.setLocationRelativeTo(null);
			super.setResizable(false);
			super.setAlwaysOnTop(true);
			super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			// Le tableau de dessin
			super.setContentPane(Utilitaire.scene);
			super.addKeyListener(Utilitaire.clavier);
			super.addMouseListener(Utilitaire.mouse);
			super.addMouseMotionListener(Utilitaire.mouse);
			
			super.setVisible(true);
		}

}
