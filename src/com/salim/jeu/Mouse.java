package com.salim.jeu;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.salim.bouton.*;

public class Mouse implements MouseMotionListener, MouseListener{

	private Rectangle hitbox;
	
	public Mouse() {
		this.hitbox = new Rectangle(0, 0, 1, 1);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		this.hitbox.x = e.getX()-5;
		this.hitbox.y = e.getY()-28;
		
		for(Bouton bouton : Bouton.getButtom) bouton.checkMouse(this.hitbox);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		for(Bouton bouton : Bouton.getButtom) bouton.checkClick(this.hitbox);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
	}
}
