package com.salim.jeu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.salim.utilitaire.Utilitaire;

public class Clavier implements KeyListener{

	
	public void keyPressed(KeyEvent e) {//Action � effectuer quand on appuie sur une touche
		if(Utilitaire.niveau.mario.isVivant()==true &&Utilitaire.niveau.getxPos()<Utilitaire.niveau.getxFin())//Mouvement possible que lorsque Mario est vivant
		{
			if(e.getKeyCode()== KeyEvent.VK_RIGHT)//Cas de la fl�che de droite
			{
					if (Utilitaire.niveau.getxPos()==-1){
						Utilitaire.niveau.setxPos(0);
						Utilitaire.niveau.setxFond1(-50);
						Utilitaire.niveau.setxFond2(750);
					}
					Utilitaire.niveau.setDx(1);//On rend dx � 1, voir dans la classe Scene
					Utilitaire.niveau.mario.setMarche(true);//On indique que mario marche
					Utilitaire.niveau.mario.setSensAvant(true);//On indique que mario va a droite
			}
			else if(e.getKeyCode()== KeyEvent.VK_LEFT)//Cas de la fl�che de gauche
			{
				Utilitaire.niveau.setDx(-1);
				Utilitaire.niveau.mario.setMarche(true);//On indique que mario marche
				Utilitaire.niveau.mario.setSensAvant(false);//On indique que mario va a gauche
			}
		
			else if(e.getKeyCode()==KeyEvent.VK_SPACE)//Cas de la fl�che de gauche
			{
				Utilitaire.niveau.mario.setSaut(true);//La variable Saut de Mario est mit � true
			}
		}
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode()==KeyEvent.VK_RIGHT || e.getKeyCode()==KeyEvent.VK_LEFT)	/*Si l'on arr�te t'appuyer 
																					sur droite ou sur gauche*/
		{
			Utilitaire.niveau.setDx(0);// Des qu'on arrete d'avancer ou reculer dx=0
			Utilitaire.niveau.mario.setMarche(false);//Pour renvoyer une image de MArio immobile
		}
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
