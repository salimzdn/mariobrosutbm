package com.salim.jeu;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.salim.bouton.*;
import com.salim.niveau.Niveau;
import com.salim.niveau.Niveau1;
import com.salim.objets.BigBlock;
import com.salim.objets.Block;
import com.salim.objets.BlockB;
import com.salim.objets.IntBlock;
import com.salim.objets.Lave;
import com.salim.objets.Lot;
import com.salim.objets.Objet;
import com.salim.objets.Piece;
import com.salim.objets.TuyauRouge;
import com.salim.personnages.Champ;
import com.salim.personnages.Mario;
import com.salim.personnages.Personnage;
import com.salim.personnages.Tortue;
import com.salim.utilitaire.Utilitaire;



public class Scene extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Image imgPrincipale;
	
	public Scene (int xfin) {//Constructeur de Scene
	
		
		super();//Juste appeler la superclass JPanel
		this.imgPrincipale=new ImageIcon(getClass().getResource("/Images/fondEcran.png")).getImage();
		
		this.setFocusable(true);
		this.addKeyListener(new Clavier());//Permet d'enregistrer les saisies clavier
		
		Thread chronoEcran = new Thread(new Chrono());//Initialisation de chronoEcran qui va effetuer la tache indiquer dans Chrono
		chronoEcran.start();//D�marage de chronoEcran
	}
	
					//M�thodes
	
	
	
	
	
	private void dessinMenuPrincipal(Graphics2D g2)
	{
		g2.drawImage(this.imgPrincipale,0,0,null);
	}

	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2=(Graphics2D) g;//On transforme en G2D
		
		switch (Utilitaire.etat)
		{
			case MENU_PRINCIPAL: 
				this.dessinMenuPrincipal(g2);
				for(int i=0;i<2;i++)
					{
						Bouton.getButtom.get(i).dessin(g2);
					}
			break;
			
			case MENU_NIVEAU:
				for(int i=2;i<3;i++)
				{
					Bouton.getButtom.get(i).dessin(g2);
				}
				
			break;
			
			case MENU_OPTION :
				;
			break;
			
			case INSTRUCTION :
				//g2.drawImage(new ImageIcon(getClass().getResource("/Images/Instruction.png")).getImage(),0,0,null );
			break;
			
			case NIVEAU_1: 
				
				System.out.println(" "+ Utilitaire.niveau.tabPersonnages.size());
				Utilitaire.niveau.dessin(g2);
				
			break;
		}
		
	}
}