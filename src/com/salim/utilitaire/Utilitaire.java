package com.salim.utilitaire;


import com.salim.jeu.Chrono;
import com.salim.jeu.Clavier;
import com.salim.niveau.Niveau;
import com.salim.niveau.Niveau1;
import com.salim.jeu.Mouse;
import com.salim.jeu.*;

public class Utilitaire {

	/*
	 * Cette class sert � stocker tout les objets principal du jeu.
	 */
	
	public static Fenetre fenetre;
	public static Scene scene = new Scene(3300);
	public static Thread chrono = new Thread(new Chrono());
	public static Clavier clavier = new Clavier();
	public static Mouse mouse = new Mouse();
	
	public static Niveau niveau =new Niveau1(3300);
	
	public static EtapeJeu etat = EtapeJeu.MENU_PRINCIPAL;//EtapeJeu.MENU_PRINCIPAL;
	//public static Camera camera = new Camera(0);
	
}
