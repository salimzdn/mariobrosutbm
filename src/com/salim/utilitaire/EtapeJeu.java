package com.salim.utilitaire;

public enum EtapeJeu {

	MENU_PRINCIPAL,
	MENU_NIVEAU,
	MENU_OPTION,
	INSTRUCTION,
	
	NIVEAU_1,
	NIVEAU_2;
}
