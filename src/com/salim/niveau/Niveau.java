package com.salim.niveau;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import com.salim.jeu.Temps;
import com.salim.objets.Lave;
import com.salim.objets.Objet;
import com.salim.objets.Piece;
import com.salim.personnages.Mario;
import com.salim.personnages.Personnage;

public class Niveau {

	public Font police;
	
	public Mario mario;
	
	public Mario getMario() {
		return mario;
	}


	public void setMario(Mario mario) {
		this.mario = mario;
	}


	private int affichagemort[]={0,0};
	private int xPos;
	private int xFin;
	private boolean victoire=false;
	
	private ImageIcon icoFond;//Fond
	private Image imgFond1;//Fond n�1
	private Image imgFond2;//Fond n�2
	
	private int dx;//Deplacement du fond, et donc de Mario
	private int xFond1;//Position abscysse de l'image de fond 1
	private int xFond2;//Position abscysse de l'image de fond 2
	private int ySOl;//hauteur du sol
	private int hauteurPlafond;//hauteur du plafond
	private int score=0;
	
	public Temps time;
	private String sTime;
	
	private ImageIcon icoDrapeau;
	private Image imgDrapeau;
	private ImageIcon icoChateauFin;
	private Image imgChateauFin;
	
	public ArrayList<Objet> tabObjets;//tableaux des objets de la carte
	public ArrayList<Personnage> tabPersonnages;
	public ArrayList<Piece> tabPieces;
	public ArrayList<Lave> tabLaves;
	//Constructeur
	
	public Niveau(int xfin)
	{
		this.xFond1=-50;//Position initiale du fond n�1
		this.xFond2=-750;//Position initiale du fond n�2
		this.dx=0;//Imobilit� de Mario
		this.xPos=-1;
		this.ySOl=293;//Hauteur du sol initial
		this.hauteurPlafond=0;//Hauteur du plafond initial
		
		icoFond= new ImageIcon(getClass().getResource("/Images/fondEcran.png"));//On dit quel image est Icofond
		imgFond1=this.icoFond.getImage();//On l'attribut a imgFond1
		imgFond2=this.icoFond.getImage();//On l'attribut a imgFond2
		
		icoChateauFin=new ImageIcon(getClass().getResource("/Images/chateauFin.png"));
		imgChateauFin=this.icoChateauFin.getImage();
		
		icoDrapeau=new ImageIcon(getClass().getResource("/Images/drapeau.png"));
		imgDrapeau=this.icoDrapeau.getImage();
		
		this.setxFin(xfin);
		
		mario = new Mario(300,245);//Initialisation de Mario
		police= new Font("Arial",Font.PLAIN,18);
		
		
		
		time=new Temps();
		this.sTime="00:00:00";
	}
	

	//Getter
	public int getxFin() {
		return xFin;
	}
	
	//Setter
	public void setxFin(int xFin) {
		this.xFin = xFin;
	}
	
	
	//M�thodes

	public String temps()
	{
		int s,m,h;
		String Ss,Sm,Sh,str;
		
		h=this.time.getTemps()/3600;
		if (h<10)
			{Sh="0"+h;}
		else
			{Sh=""+h;}
		
		m= (this.time.getTemps()-h*3600)/60;
		if (m<10)
			{Sm="0"+m;}
		else
			{Sm=""+m;}
		
		s= this.time.getTemps()-h*3600-m*60;
		if (s<10)
			{Ss="0"+s;}
		else
			{Ss=""+s;}
		
		str=Sh+":"+Sm+":"+Ss;
		return str;
	}
	

	public void deplaceFond() {
		if (this.xPos>=0 && this.xPos<this.xFin)
		{
			this.xPos=this.xPos+this.dx;
			this.xFond1=this.xFond1-this.dx;//Mise � jour de la position du fond
			this.xFond2=this.xFond2-this.dx;
		}
		
		if (this.xFond1==-800)	{this.xFond1=800;}
		else if(this.xFond2==-800)	{this.xFond2=800;}
		else if(this.xFond1==800)	{this.xFond1=-800;}
		else if(this.xFond2==800)	{this.xFond2=-800;}//Tout ca pour que l'�cran de fond se repete � l'infini
		
	}
	
	public void dessin(Graphics2D g2)
	{
			int proche=0;
			this.sTime=temps();
			
			if(this.mario.getY()+this.mario.getHauteur()>this.ySOl)
			{
				this.mario.setY(this.ySOl-this.mario.getHauteur());
			}
			
			if( proche ==0 &&this.mario.getY()+this.mario.getHauteur()<this.ySOl && this.mario.isSaut()==false && this.mario.isVivant()==true)
			{
				this.mario.setY(this.mario.getY()+1 );
			}
			
			//Detection des contacts
			
			for(int i=0;i<this.tabObjets.size();i++)
			{
				for(int j=0; j<this.tabPersonnages.size();j++)
				{
					if(this.tabPersonnages.get(j).proche(this.tabObjets.get(i))){this.tabPersonnages.get(j).contact(this.tabObjets.get(i));}
				}
				if(this.mario.isVivant()==true)
				{
					if(this.mario.proche(this.tabObjets.get(i)))
					{
						this.mario.contact(this.tabObjets.get(i),i);proche=1;
					}
				}
			}
			
			
			for (int i=0;i<this.tabPersonnages.size();i++)
			{
				for(int j=0;j<this.tabPersonnages.size();j++)
				{
					if(j!=i)
					{
						if(this.tabPersonnages.get(i).prochePNJ(this.tabPersonnages.get(j)))
						{	
							this.tabPersonnages.get(i).contactPNJ(this.tabPersonnages.get(j));
						}
					}	
				}	
			}
			
			for (int i=0;i<this.tabPersonnages.size();i++)
			{
				if(this.mario.prochePNJ(this.tabPersonnages.get(i)))
				{
					this.mario.contact(this.tabPersonnages.get(i));
				}	
			}
				
			for (int i=0;i<this.tabPieces.size();i++)
			{
				if (this.mario.proche(this.tabPieces.get(i)))
				{
					this.mario.contactPiece(this.tabPieces.get(i));
				}
			}
			
			System.out.println(this.score);
			
			
			for(int i=0;i<this.tabLaves.size();i++)
			{
				if (this.mario.proche(this.tabLaves.get(i)))
				{
					//this.mario.contactLave(this.tabLaves.get(i));
				}
			}
			
			//Mise � jour des positions
			
			this.deplaceFond();//On met � jour la position du fond avant de l'afficher
			
			for (int i=0;i<this.tabPersonnages.size();i++)
			{
				this.tabPersonnages.get(i).deplacement();//On met � jour la position des personnages avant de l'afficher
			}
			
			for (int i=0;i<this.tabObjets.size();i++)
			{
				this.tabObjets.get(i).deplacement();//On met � jour la position des objets avant de l'afficher
			}
			
			for (int i=0;i<this.tabPieces.size();i++)
			{
				this.tabPieces.get(i).deplacement();//On met � jour la position des objets avant de l'afficher
			}
			
			for(int i=0;i<this.tabLaves.size();i++)
			{
				this.tabLaves.get(i).deplacement();
			}
			
			if(this.xPos>=this.xFin)
			{
				this.victoire=true;
			}
			
			//Dessin
			
			g2.setFont(police);
					
				
			
			g2.drawImage(imgFond1, this.xFond1, 0, null);//On dessine le fond
			g2.drawImage(imgFond2, this.xFond2, 0, null);
			
			g2.drawString("Score :" + this.score,460 , 25);	
			
			g2.drawString(this.sTime, 25, 25);
			System.out.println(this.time.getTemps());
			
			for (int i=0;i<this.tabPersonnages.size();i++)//Affichage des perso
			{
				if(this.tabPersonnages.get(i).isVivant()==true)//Cas des personnage vivant
				{
					g2.drawImage(this.tabPersonnages.get(i).marche(this.tabPersonnages.get(i).getNom(),45 ),
					this.tabPersonnages.get(i).getX(), this.tabPersonnages.get(i).getY(), null);
					
				}
				else//Cas des personnage mort
				{
					if(this.tabPersonnages.get(i).getNom()=="tortue")//Cas de la tortue
					{
						if(this.affichagemort[i]<=1000)
						{
							g2.drawImage(this.tabPersonnages.get(i).marche(this.tabPersonnages.get(i).getNom(),45 ), 
											this.tabPersonnages.get(i).getX(), this.tabPersonnages.get(i).getY(), null);
							this.affichagemort[i]=this.affichagemort[i]+1;
						}
						else if(this.affichagemort[i]==1001)
						{
							this.tabPersonnages.get(i).setX(100);
							this.affichagemort[i]=this.affichagemort[i]+1;
						}
						else{}
					}
					else//Cas du champignon
					{
						if(this.affichagemort[i]<=200)
						{
							g2.drawImage(this.tabPersonnages.get(i).marche(this.tabPersonnages.get(i).getNom(),45 ), 
											this.tabPersonnages.get(i).getX(), this.tabPersonnages.get(i).getY(), null);
							this.affichagemort[i]=this.affichagemort[i]+1;
						}
						else if(this.affichagemort[i]==201)
						{
							this.tabPersonnages.get(i).setX(100);
							this.affichagemort[i]=this.affichagemort[i]+1;
						}
						else{}
					}
					
				}
			}
			
			for (int i=0;i<this.tabObjets.size();i++)//Affichage des objets
			{
				g2.drawImage(this.tabObjets.get(i).getImgObjet(), this.tabObjets.get(i).getX(), this.tabObjets.get(i).getY(), null);
			}
			
			for (int i=0;i<this.tabLaves.size();i++)//Affichage des lave
			{
				g2.drawImage(this.tabLaves.get(i).getImgObjet(), this.tabLaves.get(i).getX(), this.tabLaves.get(i).getY(), null);
			}
			
			for (int i=0;i<this.tabPieces.size();i++)//Affichage des piece
			{
				if(this.tabPieces.get(i).isChope()==false)
				{
					System.out.println("piece num�ro "+i+" pas chop�");
					g2.drawImage(this.tabPieces.get(i).getImgObjet(), this.tabPieces.get(i).getX(), this.tabPieces.get(i).getY(), null);
				}
			}
			
			if(this.victoire==false)
			{
				if(mario.isSaut()==true)//Affichage de mario
				{
					g2.drawImage(this.mario.saute(),300, this.mario.getY(), null);
				}
				else
				{
					g2.drawImage(this.mario.marche("mario",25), 300, this.mario.getY(), null);
				}
			}
			else if(this.victoire==true && this.xPos<this.xFin)
			{
				g2.drawImage(this.mario.victoire(), 300, this.mario.getY(), null);
			}
			
			
			if (this.xPos>2700)//Affichage de la fin
			{
				g2.drawImage(this.imgDrapeau, 3600-this.xPos, 115, null);
				g2.drawImage(this.imgChateauFin, 3650-this.xPos, 115, null);
			}
			
			if(this.mario.isVivant()==false)
			{
				g2.drawString("Game Over",150 , 90);
			}
		System.out.println("nbre de piece"+this.tabPieces.size());
	}


	public Font getPolice() {
		return police;
	}


	public void setPolice(Font police) {
		this.police = police;
	}


	public int getxPos() {
		return xPos;
	}


	public void setxPos(int xPos) {
		this.xPos = xPos;
	}


	public boolean isVictoire() {
		return victoire;
	}


	public void setVictoire(boolean victoire) {
		this.victoire = victoire;
	}


	public int getDx() {
		return dx;
	}


	public void setDx(int dx) {
		this.dx = dx;
	}


	public int getxFond1() {
		return xFond1;
	}


	public void setxFond1(int xFond1) {
		this.xFond1 = xFond1;
	}


	public int getxFond2() {
		return xFond2;
	}


	public void setxFond2(int xFond2) {
		this.xFond2 = xFond2;
	}


	public int getySOl() {
		return ySOl;
	}


	public void setySOl(int ySOl) {
		this.ySOl = ySOl;
	}


	public int getHauteurPlafond() {
		return hauteurPlafond;
	}


	public void setHauteurPlafond(int hauteurPlafond) {
		this.hauteurPlafond = hauteurPlafond;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public String getsTime() {
		return sTime;
	}


	public void setsTime(String sTime) {
		this.sTime = sTime;
	}
	
	
	
}
