package com.salim.niveau;

import java.util.ArrayList;

import com.salim.objets.BigBlock;
import com.salim.objets.Block;
import com.salim.objets.BlockB;
import com.salim.objets.IntBlock;
import com.salim.objets.Lave;
import com.salim.objets.Objet;
import com.salim.objets.TuyauRouge;
import com.salim.personnages.Champ;
import com.salim.personnages.Personnage;
import com.salim.personnages.Tortue;

public class Niveau2 extends Niveau {

	public Niveau2(int xfin) {
		super(xfin);
		// TODO Auto-generated constructor stub
	}


	public TuyauRouge tuyaurouge1;
	
	public Block block1;
	public Block block2;
	public Block block3;
	public Block block4;
	public Block block5;
	public Block block6;
	public Block block7;
	public Block block8;
	public Block block9;
	public Block block10;
	public Block block11;
	public Block block12;
	public Block block13;
	public Block block14;
	public Block block15;
	public Block block16;
	public Block block17;
	public Block block18;
	public Block block19;
	public Block block20;
	public Block block21;
	public Block block22;
	public Block block23;
	public Block block24;
	public Block block25;
	public Block block26;
	public Block block27;


	public BigBlock bigblock1;
	public BigBlock bigblock2;

	public BigBlockrouge bigblockR1;
	public BigBlockrouge bigblockR2;

	public BigBlockbleu bigblockB1;

	public IntBlock intblock1;

	
	public BlockB blockB1;
	public BlockB blockB2;
	public BlockB blockB3;
	public BlockB blockB4;
	public BlockB blockB5;
	public BlockB blockB6;
	public BlockB blockB7;
	public BlockB blockB8;
	public BlockB blockB9;
	public BlockB blockB10;
	public BlockB blockB11;
	public BlockB blockB12;
	public BlockB blockB13;
	public BlockB blockB14;
	public BlockB blockB15;
	public BlockB blockB16;
	public BlockB blockB17;
	public BlockB blockB18;
	public BlockB blockB19;
	public BlockB blockB20;
	public BlockB blockB21;
	public BlockB blockB22;
	public BlockB blockB23;
	public BlockB blockB24;
	public BlockB blockB25;
	public BlockB blockB26;
	public BlockB blockB27;
	public BlockB blockB28;


public Lave Lave1;
	public Lave Lave2;
	public Lave Lave3;
	public Lave Lave4;
	public Lave Lave5;
	public Lave Lave6;
	public Lave Lave7;
	public Lave Lave8;
	public Lave Lave9;
	public Lave Lave10;
	public Lave Lave11;
	public Lave Lave12;
	public Lave Lave13;
	public Lave Lave14;
	public Lave Lave15;
	public Lave Lave16;
	public Lave Lave17;
	public Lave Lave18;
	public Lave Lave19;
	public Lave Lave20;

	public TuyauVertCorps tuyauVC1;
	public TuyauVertTete tuyauVT1;

	public Tortue tortue1;
	public Tortue tortue2;
	public Tortue tortue3;
	public Tortue tortue4;
	
	public Champ champ1;
	public Champ champ2;
	public Champ champ3;
	public Champ champ4;
	public Champ champ5;
	
	public void remplissageObjet()
	{
		this.tabObjets= new ArrayList<Objet>();
		tuyaurouge1= new TuyauRouge(1290,230);

		tuyauVC1 = new TuyauVertCorps (1440,230) ;
		tuyauVT1 =new TuyauVertTete (1440,200) ;
		


		/*2*2blocs*/
		block1=new Block(500,190);
		block2 = new Block(530,190);
		block3 = new Block (620,190);
		block4 = new Block (650,190);
		/*blocs au dessus du lac de lave*/
		block5 = new Block (1050,130);
		block6 = new Block (1080,100);
		block7 = new Block (1110,130);
		block8 = new Block (1140,70);
		/*murs extremités lac de lave 2*/
		block9 = new Block (2190,220);
		block10 = new Block (2190,190);
		block11 = new Block (2280,190);								
		block12 = new Block (2370,220);
		block13 = new Block (2370,190);
		/*parcours fin*/
		block14 = new Block (3150,250);								
		block15 = new Block (3150,130);
		block16 = new Block (3180,190);
		block17 = new Block (3210,250);								
		block18 = new Block (3270,250);
		block19 = new Block (3270,160);
		block20 = new Block (3300,190);								
		block21 = new Block (3330,250);
		block22 = new Block (3330,190);
		block23 = new Block (3390,250);								
		block24 = new Block (3390,160);
		block25 = new Block (3450,250);
		block26 = new Block (3480,130);								
		block27 = new Block (3540,160);
		
		
	


				
		
		


		/*1 assemblage bigbloc 1*/
		bigblock1 = new BigBlock (900,250);
		bigblockR1 = new BigBlockrouge (930,250); 
		/*assemblage bligbloc 2*/
		bigblock2 = new BigBlock (1650,250);
		bigblockR2 = new BigBlockrouge (1710,250);
		bigblockB1 = new BigBlockbleu (1740,250);

		/*bloc avant lave*/
		blockB1 = new BlockB (990,250);
		/*murs 2eme lac de lave*/
		blockB2 = new BlockB (2190,250);
		blockB3 = new BlockB (2370,250);
		 		/*escalier montant*/
		blockB4 = new BlockB (2730,250);
		blockB5 = new BlockB (2760,250);
		blockB6 = new BlockB (2760,220);
		blockB7 = new BlockB (2790,250);
		blockB8 = new BlockB (2790,220);
		blockB9 = new BlockB (2790,190);
		/*escalier descendant*/
		blockB10 = new BlockB (2880,250);
		blockB11 = new BlockB (2880,220);
		blockB12 = new BlockB (2880,190);
		blockB13 = new BlockB (2910,250);
		blockB14 = new BlockB (2910,220);
		blockB15 = new BlockB (2940,250);
		/*parcours barre de fin*/
		blockB16 = new BlockB (3180,250);
		blockB17 = new BlockB (3240,250);
		blockB18 = new BlockB (3300,250);
		blockB19 = new BlockB (3360,250);
		blockB20 = new BlockB (3420,250);
		blockB21 = new BlockB (3480,250);
		blockB22 = new BlockB (3510,250);
		blockB23 = new BlockB (3510,220);
		blockB24 = new BlockB (3660,250);
		blockB25 = new BlockB (3660,220);
		blockB26 = new BlockB (3660,190);
		blockB27 = new BlockB (3660,100);
		blockB28 = new BlockB (3660,70);
		





		/*premier intblock au dessus bigblocrouge*/
		intblock1 = new IntBlock(930,40);
		
		
		
		
		this.tabObjets.add(this.block1);
		this.tabObjets.add(this.block2);
		this.tabObjets.add(this.block3);
		this.tabObjets.add(this.block4);
		this.tabObjets.add(this.block5);
		this.tabObjets.add(this.block6);
		this.tabObjets.add(this.block7);
		this.tabObjets.add(this.block8);
		this.tabObjets.add(this.block9);
		this.tabObjets.add(this.block10);
		this.tabObjets.add(this.block11);
		this.tabObjets.add(this.block12);
		this.tabObjets.add(this.block13);
		this.tabObjets.add(this.block14);
		this.tabObjets.add(this.block15);
		this.tabObjets.add(this.block16);
		this.tabObjets.add(this.block17);
		this.tabObjets.add(this.block18);
		this.tabObjets.add(this.block19);
		this.tabObjets.add(this.block20);
		this.tabObjets.add(this.block21);
		this.tabObjets.add(this.block22);
		this.tabObjets.add(this.block23);
		this.tabObjets.add(this.block24);
		this.tabObjets.add(this.block25);
		this.tabObjets.add(this.block26);
		this.tabObjets.add(this.block27);


		
		
		this.tabObjets.add(this.blockB1);
		this.tabObjets.add(this.blockB2);
		this.tabObjets.add(this.blockB3);
		this.tabObjets.add(this.blockB4);
		this.tabObjets.add(this.blockB5);
		this.tabObjets.add(this.blockB6);
		this.tabObjets.add(this.blockB7);
		this.tabObjets.add(this.blockB8);
		this.tabObjets.add(this.blockB9);
		this.tabObjets.add(this.blockB10);
		this.tabObjets.add(this.blockB11);
		this.tabObjets.add(this.blockB12);
		this.tabObjets.add(this.blockB13);
		this.tabObjets.add(this.blockB14);
		this.tabObjets.add(this.blockB15);
		this.tabObjets.add(this.blockB16);
		this.tabObjets.add(this.blockB17);
		this.tabObjets.add(this.blockB18);
		this.tabObjets.add(this.blockB19);
		this.tabObjets.add(this.blockB20);
		this.tabObjets.add(this.blockB21);
		this.tabObjets.add(this.blockB22);
		this.tabObjets.add(this.blockB23);
		this.tabObjets.add(this.blockB24);
		this.tabObjets.add(this.blockB25);
		this.tabObjets.add(this.blockB26);
		this.tabObjets.add(this.blockB27);
		this.tabObjets.add(this.blockB28);

		this.tabObjets.add(this.Lave1);
		this.tabObjets.add(this.Lave2);
		this.tabObjets.add(this.Lave3);
		this.tabObjets.add(this.Lave4);
		this.tabObjets.add(this.Lave5);
		this.tabObjets.add(this.Lave6);
		this.tabObjets.add(this.Lave7);
		this.tabObjets.add(this.Lave8);
		this.tabObjets.add(this.Lave9);
		this.tabObjets.add(this.Lave10);
		this.tabObjets.add(this.Lave11);
		this.tabObjets.add(this.Lave12);
		this.tabObjets.add(this.Lave13);
		this.tabObjets.add(this.Lave14);
		this.tabObjets.add(this.Lave15);
		this.tabObjets.add(this.Lave16);
		this.tabObjets.add(this.Lave17);
		this.tabObjets.add(this.Lave18);
		this.tabObjets.add(this.Lave19);
		this.tabObjets.add(this.Lave20);





		this.tabObjets.add(this.bigblock1);
		this.tabObjets.add(this.bigblock2);
		this.tabObjets.add(this.bigblockR1);
		this.tabObjets.add(this.bigblockR2);
		this.tabObjets.add(this.bigblockB1);
	
		
		this.tabObjets.add(this.intblock1);

		this.tabObjets.add(this.tuyaurouge1);
		this.tabObjets.add(this.tuyauVT1);
		this.tabObjets.add(this.tuyauVC1);

	}
	
	public void remplissagePersonnage(){
		
		tabPersonnages= new ArrayList<Personnage>();
		
		
		tortue1= new Tortue(1860,245);
		tortue2=new Tortue (1920,245);
		tortue3= new Tortue (3840,245);
		tortue4 = new Tortue (4200,245);
		
		champ1= new Champ(560,263);
		champ2 = new Champ (680,263);
		champ3 = new Champ (1440,150);
		champ4 = new Champ (3990,263);
		champ5 = new Champ (4080,263);
	}

	public void remplissageLave(){
	/*lac lave1*/
	Lave1 = new Lave (1020,250) ;
	Lave2 = new Lave (1050,250) ;
	Lave3 = new Lave (1080,250) ;
	Lave4 = new Lave (1110,250) ;
	Lave5 = new Lave (1140,250) ;
	Lave6 = new Lave (1170,250) ;
	Lave7 = new Lave (1200,250) ;
	Lave8 = new Lave (1230,250) ;
	Lave9 = new Lave (1260,250) ;
	/*lac lave 2*/
	Lave10 = new Lave (2200,250) ;
	Lave11 = new Lave (2250,250) ;
	Lave12 = new Lave (2280,250) ;
	Lave13 = new Lave (2310,250) ;
	Lave14 = new Lave (2340,250) ;
	/*lac de lave 3*/
	Lave15 = new Lave (2820,250) ;
	Lave16 = new Lave (2850,250) ;
	/*lac lave4*/
	Lave17 = new Lave (3540,250) ;
	Lave18 = new Lave (3570,250) ;
	Lave19 = new Lave (3600,250) ;
	Lave20 = new Lave (3630,250) ;
		}
	
	
	public void remplissagePiece(){
		
		}
	


	
}
