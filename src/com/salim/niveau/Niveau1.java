package com.salim.niveau;

import java.util.ArrayList;

import com.salim.objets.BigBlock;
import com.salim.objets.Block;
import com.salim.objets.BlockB;
import com.salim.objets.IntBlock;
import com.salim.objets.Lave;
import com.salim.objets.Lot;
import com.salim.objets.Objet;
import com.salim.objets.Piece;
import com.salim.objets.TuyauRouge;
import com.salim.personnages.Champ;
import com.salim.personnages.Personnage;
import com.salim.personnages.Tortue;

public class Niveau1 extends Niveau{

	public Tortue tortue;
	public Champ champ;
	
	public Champ ichamp1;
	
	public TuyauRouge tuyauRouge1;
	public TuyauRouge tuyauRouge2;
	public TuyauRouge tuyauRouge3;
	public TuyauRouge tuyauRouge4;
	public TuyauRouge tuyauRouge5;
	public TuyauRouge tuyauRouge6;
	public TuyauRouge tuyauRouge7;
	public TuyauRouge tuyauRouge8;
	
	public Block block1;
	public Block block2;
	public Block block3;
	public Block block4;
	public Block block5;
	public Block block6;
	public Block block7;
	public Block block8;
	public Block block9;
	public Block block10;
	public Block block11;
	public Block block12;
	public Block block13;
	public Block block14;
	public Block block15;
	public Block block16;
	public Block block17;
	public Block block18;
	public Block block19;
	public Block block20;
	public Block block21;
	public Block block22;
	public Block block23;
	public Block block24;
	public Block block25;
	public Block block26;
	public Block block27;
	public Block block28;
	public Block block29;
	public Block block30;
	public Block block31;
	public Block block32;
	public Block block33;
	public Block block34;
	
	public BigBlock bigblock1;
	public BigBlock bigblock2;
	public BigBlock bigblock3;
	public BigBlock bigblock4;
	public BigBlock bigblock5;
	public BigBlock bigblock6;
	public BigBlock bigblock7;
	
	public IntBlock intblock1;
	public IntBlock intblock2;
	
	public BlockB blockB1;
	public BlockB blockB2;
	public BlockB blockB3;
	public BlockB blockB4;
	public BlockB blockB5;
	public BlockB blockB6;
	public BlockB blockB7;
	public BlockB blockB8;
	public BlockB blockB9;
	public BlockB blockB10;
	public BlockB blockB11;
	public BlockB blockB12;
	public BlockB blockB13;
	public BlockB blockB14;
	public BlockB blockB15;
	public BlockB blockB16;
	public BlockB blockB17;
	public BlockB blockB18;
	public BlockB blockB19;
	public BlockB blockB20;
	public BlockB blockB21;
	public BlockB blockB22;
	public BlockB blockB23;
	public BlockB blockB24;
	public BlockB blockB25;
	public BlockB blockB26;

	public Lave lave1;
	public Lave lave2;
	
	public Lot lot1;
	public Lot lot1_2;
	public Lot lote1_1;
	public Lot lote1_2;
	public Lot lote1_3;
	public Lot lote1_4;
	public Lot lot2;
	public Lot lot3;
	public Lot lot4_1;
	public Lot lot4_2;
	public Lot lot4_3;
	public Lot lot5;
	public Lot lot6;
	public Lot lot7;
	public Lot lot8;
	
	public Piece piece1;
	public Piece piece2;


	public Niveau1(int xfin) {
		
		super(xfin);
		remplissageObjet();
		remplissagePersonnage();
		remplissagePiece();
		remplissageLave();
		
	}

	protected void remplissageObjet() {
		
		this.tabObjets= new ArrayList<Objet>();
		
		tuyauRouge3= new TuyauRouge(1700,230);
		
		/*premiere barre*/
		block1=new Block(400,175);
		block2 = new Block(430,175);
		block3 = new Block (460,175);
		block4 = new Block (520,175);
		lot1= new Lot(400,175,30,148);
		
		block5 = new Block (550,145);
		lot1_2=new Lot(550,145,30,30);
		System.out.println("x:"+this.lot1.getX()+"\ny:"+this.lot1.getY()+"\nlargeur:"+this.lot1.getLargeur()+"\nhauteur"+this.lot1.getHauteur());
		/*deuxieme barre*/
		block6 = new Block (880,145);
		block7 = new Block (910,145);
		block8 = new Block (940,145);
		block9 = new Block (970,145);
		lot2=new Lot(880,145,30,148);
		
		/*escalier numero 1 avant saut*/
		blockB1 = new BlockB (620,263);
		blockB2 = new BlockB (650,233);
		blockB3 = new BlockB (680,203);
		blockB4 = new BlockB (710,173);
		blockB5 = new BlockB (710,263);
		blockB6 = new BlockB (710,233);
		blockB7 = new BlockB (710,203);
		blockB8 = new BlockB (680,263);
		blockB9 = new BlockB (650,263);
		blockB10 = new BlockB (680,233);
		
		
		//lote1_1=new Lot(620,263,)
		/*troisieme barre*/
		block10 =new Block (950,85);
		block11 =new Block (980,85);
		block12 =new Block (1010,85);
		block13 =new Block (1040,85);
		lot3=new Lot(950,85,30,148);
		/*bare 4 incomplète */
		block14 =new Block (1310,175);
		lot4_1=new Lot(1310,175,30,58);
		
		block15 =new Block (1370,175);
		lot4_2=new Lot(1370,175,30,58);
		
		block16 =new Block (1430,175);		
		block17 =new Block (1460,175);
		lot4_3=new Lot(1430,175,30,88);
		
		/*
		 * ESCALIER PILONNE
		 */
		/*marche 1 */
		block18 =new Block (1900,235);
		/*marche 2 */
		block19 =new Block (2020,235);
		block20 =new Block (2020,205);
		/*marche 3 */
		block21 =new Block (2110,235);
		block22 =new Block (2110,205);
		block23 =new Block (2110,175);
		/*marche 4 */
		block24 =new Block (2230,235);
		block25 =new Block (2230,205);
		block26 =new Block (2230,175);
		block27 =new Block (2230,145);
		/*bloc seul avant pont*/
		block28 =new Block (2350,235);
		/*U*/
		block29 =new Block (2800,235);
		block30 =new Block (2800,205);
		block31 =new Block (2860,235);
		block32 =new Block (2920,235);
		block33 =new Block (2980,235);
		block34 =new Block (2980,205);
		
		/*1 bigblock apres saut*/
		bigblock1 = new BigBlock (980,235);
		bigblock2 = new BigBlock (1550,235);
		/*pont*/
		bigblock3 = new BigBlock (2410,175);
		bigblock4 = new BigBlock (2590,175);
		/*escalier*/
		bigblock5 = new BigBlock (3040,235);
		bigblock6 = new BigBlock (3100,235);
		bigblock7 = new BigBlock (3100,175);
		
		
		
		/*mur avant parcour*/
		blockB11 = new BlockB (1130,235);
		blockB12 = new BlockB (1130,205);
		blockB13 = new BlockB (1130,175);
		blockB14 = new BlockB (1130,145);
		blockB15 = new BlockB (1130,115);
		/*mur au dessus Big Block 2*/
		blockB16 = new BlockB (1550,115);
		blockB17 = new BlockB (1550,85);
		blockB18 = new BlockB (1550,55);
		blockB19 = new BlockB (1550,25);
		blockB20 = new BlockB (1580,115);
		blockB21 = new BlockB (1580,85);
		blockB22 = new BlockB (1580,55);
		blockB23 = new BlockB (1580,25);
		/* pont */
		blockB23 = new BlockB (2470,145);
		blockB24 = new BlockB (2500,145);
		blockB25 = new BlockB (2530,145);
		blockB26 = new BlockB (2560,145);
	
		/*premier intblock dans 1ere barre*/
		intblock1 = new IntBlock(490,175);
		/*deuxieme intbloc avant mur, apres bigbloc1*/
		intblock2 =new IntBlock (1100,175);
		
		
		
		
		this.tabObjets.add(this.block1);
		this.tabObjets.add(this.block2);
		this.tabObjets.add(this.block3);
		this.tabObjets.add(this.block4);
		this.tabObjets.add(this.block5);
		this.tabObjets.add(this.block6);
		this.tabObjets.add(this.block7);
		this.tabObjets.add(this.block8);
		this.tabObjets.add(this.block9);
		this.tabObjets.add(this.block10);
		this.tabObjets.add(this.block11);
		this.tabObjets.add(this.block12);
		this.tabObjets.add(this.block13);
		this.tabObjets.add(this.block14);
		this.tabObjets.add(this.block15);
		this.tabObjets.add(this.block16);
		this.tabObjets.add(this.block17);
		this.tabObjets.add(this.block18);
		this.tabObjets.add(this.block19);
		this.tabObjets.add(this.block20);
		this.tabObjets.add(this.block21);
		this.tabObjets.add(this.block22);
		this.tabObjets.add(this.block23);
		this.tabObjets.add(this.block24);
		this.tabObjets.add(this.block25);
		this.tabObjets.add(this.block26);
		this.tabObjets.add(this.block27);
		this.tabObjets.add(this.block28);
		this.tabObjets.add(this.block29);
		this.tabObjets.add(this.block30);
		this.tabObjets.add(this.block31);
		this.tabObjets.add(this.block32);
		this.tabObjets.add(this.block33);
		this.tabObjets.add(this.block34);
	
		
		
		this.tabObjets.add(this.blockB1);
		this.tabObjets.add(this.blockB2);
		this.tabObjets.add(this.blockB3);
		this.tabObjets.add(this.blockB4);
		this.tabObjets.add(this.blockB5);
		this.tabObjets.add(this.blockB6);
		this.tabObjets.add(this.blockB7);
		this.tabObjets.add(this.blockB8);
		this.tabObjets.add(this.blockB9);
		this.tabObjets.add(this.blockB10);
		this.tabObjets.add(this.blockB11);
		this.tabObjets.add(this.blockB12);
		this.tabObjets.add(this.blockB13);
		this.tabObjets.add(this.blockB14);
		this.tabObjets.add(this.blockB15);
		this.tabObjets.add(this.blockB16);
		this.tabObjets.add(this.blockB17);
		this.tabObjets.add(this.blockB18);
		this.tabObjets.add(this.blockB19);
		this.tabObjets.add(this.blockB20);
		this.tabObjets.add(this.blockB21);
		this.tabObjets.add(this.blockB22);
		this.tabObjets.add(this.blockB23);
		this.tabObjets.add(this.blockB24);
		this.tabObjets.add(this.blockB25);
		this.tabObjets.add(this.blockB26);
	
	
	
		this.tabObjets.add(this.bigblock1);
		this.tabObjets.add(this.bigblock2);
		this.tabObjets.add(this.bigblock3);
		this.tabObjets.add(this.bigblock4);
		this.tabObjets.add(this.bigblock5);
		this.tabObjets.add(this.bigblock6);
		this.tabObjets.add(this.bigblock7);
		
		this.tabObjets.add(this.intblock1);
		this.tabObjets.add(this.intblock2);
	
		this.tabObjets.add(this.tuyauRouge3);
	
	}

	protected void remplissagePersonnage() {
		tabPersonnages= new ArrayList<Personnage>();
		
		tortue= new Tortue(700,245);
		
		champ= new Champ(1280,263);
		
		this.tabPersonnages.add(this.tortue);
		
		this.tabPersonnages.add(this.champ);
	}

	protected void remplissagePiece() {
		tabPieces=new ArrayList<Piece>();
		
		piece1=new Piece (430,148);
		piece2=new Piece (550,118);
		
		this.tabPieces.add(this.piece1);
		this.tabPieces.add(this.piece2);
		
	}

	protected void remplissageLave() {
		tabLaves=new ArrayList<Lave>();
		
		lave1= new Lave(400,280);
		lave2=new Lave (450,280);
		
		this.tabLaves.add(this.lave1);
		this.tabLaves.add(this.lave2);
		
	}

}
